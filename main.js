const STATION_MAP_ENDPOINT = "data/station_map_joined.csv?v=4";

const GET = new Proxy(new URLSearchParams(window.location.search), {
	get: (searchParams, prop) => searchParams.get(prop),
});

const COLORIZERS = {
	duration() {
		var range = visDataView.map(e => e.ride.trajanje).range();
		var lm = range.max - range.min;
		
		for (let item of visDataView.get()) {
			let itm = visTimeline.itemSet.getItemById(item.id);
			if (!itm || !itm.dom) continue;
			let $item = itm.dom.box;
			let per = (item.ride.trajanje - range.min) / lm;
			//$item.dataset.per = per;
			$item.style.backgroundColor = getColorForPercentage(1 - per);
		}
	},
	type() {
		let COLORS = {
			bus: "#FCBE49",
			train: "#70D4FF",
			rideshare: "gray",
		}
		for (let item of Object.values(visTimeline.itemSet.items)) {
			let $item = item.dom.box;
			$item.style.backgroundColor = COLORS[item.data.ride.provider.type];
		}
	},
	provider() {
		for (let item of Object.values(visTimeline.itemSet.items)) {
			let $item = item.dom.box;
			$item.style.backgroundColor = item.data.ride.provider.color;
		}
	},
}

function updateColoring() {
	let coloring = document.querySelector("[name=coloring]").value;
	COLORIZERS[coloring]();
}

const FILTER = {
	providers: {},
	maxDuration: 500,
	hideRedundant: true,
}

function refresh() {
	visDataView.refresh();
	updateColoring();
}

function evt_providerLoaded() {
	refresh();
}

function filterFunc(item) {
	if (FILTER.providers[item.provider] === false)
		return false;
	
	for (let provider of Object.values(providers)) 
		if (provider.filter_item(item) === false)
			return false;

	if (item.ride.trajanje > FILTER.maxDuration)
		return false;
	if (item.ride.redundant && FILTER.hideRedundant)
		return false;
	return true;
}

async function load_times(params) {
	let {from, to} = params;

	// Add group
	params.groupId = `${from.name} - ${to.name}`;
	if (! visTimeline.groupsData.map(e => e.id).includes(params.groupId) )
		groupsDataSet.add({
			id: params.groupId,
			params: params
		})
			
	// Update group bar width
	$timeline.style.setProperty("--group-bar-width", document.querySelector(".vis-panel.vis-left").clientWidth + "px")
	
	// Zoom to range
	{
		let today = new Date(params.date);
		today.setHours(-3);
		today.setMinutes(0);
		today.setSeconds(0);
		let tomorrow = new Date(today);
		tomorrow.setDate(today.getDate() + 2);
		tomorrow.setHours(3);
		visTimeline.setWindow(today, tomorrow);
	}

	// Actually start loading in data
	for (let provider in providers) {
		console.debug(provider, from[provider + "_id"], to[provider + "_id"])
		if (from[provider + "_id"] && to[provider + "_id"]) {
			toLoad += 1;
			providers[provider].loadDataset(params).then(items => {
				visDataSet.add(items)
				btn_undisable();
				evt_providerLoaded();
			}).catch(e => {
				btn_undisable();
				console.error(e);
			});
		}
	}
}

let toLoad = 0;
function btn_undisable() {
	toLoad -= 1;
	if (toLoad == 0)
		$submit_btn.disabled = false;
}

async function on_submit(e) {
	e.preventDefault();

	let from = window.station_map[$select_from.value];
	let to = window.station_map[$select_to.value];
	let date = new Date($date_input.value);

	$submit_btn.disabled = true;
	await load_times({from, to, date});
}

function item_onDblClick (e) {
	console.debug(e)
}

function normalize(s) {
	return s.toLowerCase().replace(/š/g, 's').replaceAll(/č/g, 'c').replaceAll(/ž/g, 'z')
}

async function init_form() {
	let providerFilters = "";
	for (let provider in providers) {
		providerFilters += html`<label class="badge ${provider}"><input type="checkbox" data-provider="${provider}" onchange="FILTER.providers[this.dataset.provider] = this.checked; refresh();" checked></label>`
	}
	document.getElementById("provider-filters").innerHTML = providerFilters;

	let resp = await fetch(STATION_MAP_ENDPOINT);
	let text = await resp.text();
	let stations = csv_parse(text);

	window.station_map = {};
	for (let station of stations) {
		if (station.id)
			station_map[station.id] = station;
	}

	window.$select_to = document.getElementById("select_to");
	window.$select_from = document.getElementById("select_from");
	window.$submit_btn = document.getElementById("submit_btn");
	window.$date_input = document.getElementById("date_input");
	window.$form = document.querySelector("#panel form");
	
	$form.addEventListener("submit", on_submit);
	$date_input.valueAsDate = new Date();

	let selectData = [];
	for (let id in window.station_map) {
		let station = window.station_map[id];
		
		selectData.push({
			id: station.id,
			text: normalize(station.name),
			innerHTML: Templates.selectItem(station),
		});
	}
	


	let select2options = {
		// data: selectData,
		language: 'sl',
		escapeMarkup: function(markup) {
			return markup;
		},
		templateResult: function(data) {
			return data.innerHTML;
		},
		templateSelection: function(data) {
			return data.innerHTML;
		},
		ajax: {
			transport: function (params, success, failure) {
				let PAGE_SIZE = 100;

				let search = normalize(params.data.term || "");
				let page = (params.data.page || 1) - 1;
				
				setTimeout(() => {
					let limit = Math.min(page * PAGE_SIZE + PAGE_SIZE, selectData.length);
					let results = [];
					for (let i = page * PAGE_SIZE; i < limit; i++) {
						let item = selectData[i];
						if (item.text.startsWith(search)) {
							results.push(item);
						}
					}
					success({
						results: results,
						pagination: {
							more: limit != selectData.length,
						}
					})
				}, 0);
			}
		}
	};
	$($select_from).select2(select2options);
	$($select_to).select2(select2options);

	$(document).on('select2:open', () => {
		document.querySelector('.select2-search__field').focus();
	  });
	

	document.getElementById("swap").onclick = () => {
		let val = $($select_from).val();
		$($select_from_ss).val($($select_to).val());
		$($select_to_ss).val(val);
	}


}


function init_timeline() {
	$timeline = document.getElementById('timeline');
	let today = new Date();
	let options = {
		height: "100%",
		zoomMax: 1000*60*60*24*20,
		zoomMin: 1000*60*15,
		min: new Date(today.getFullYear(), today.getMonth()-12, 1),
		max: new Date(today.getFullYear(), today.getMonth()+12, 0),
		//center: today,
		showCurrentTime: true,
		orientation: {
			axis: "top",
			item: "top"
		},
		horizontalScroll: true,
		verticalScroll: false,
		preferZoom: true,
		template: Templates.item,
		tooltip: {
			template: Templates.tooltip
		},
		groupTemplate: Templates.group
	};

	// The timeline need at least one item to init
	window.visDataSet = new vis.DataSet([{
		start: new Date(),
		content: 'Loading...',
		ride: {},
	}]);
	window.visDataView = new vis.DataView(visDataSet, {filter: filterFunc});
	
	visDataSet.remove(visDataSet.getIds()[0])
	
	window.groupsDataSet = new vis.DataSet([]);
	window.visTimeline = new vis.Timeline($timeline, visDataView, groupsDataSet, options);
	visTimeline.moveTo(today);

	// visTimeline.on("select", onSelect);

	$timeline.onwheel = function(event) {
		if (event.deltaX != 0) {
			visTimeline.options.preferZoom = false;
			visTimeline.options.zoomKey = "ctrlKey";
		} else if (event.deltaY != 0) {
			visTimeline.options.preferZoom = true;
			visTimeline.options.zoomKey = ""
		}
		visTimeline.setOptions(visTimeline.options)
	}

	visTimeline.on('doubleClick', function (props) {
		let item = visDataSet.get(props.item)
		item.ride.naziv = item.group
		let ical = Templates.icalFile([item.ride]);
		downloadString(ical, "text/icalendar", "ojpp.ics");
		props.event.preventDefault();
	  });

}

function deleteGroup(groupId) {
	visDataSet.remove(visDataSet.map(e => e).filter(e => e.group == groupId).map(e => e.id))
	groupsDataSet.remove(groupId);
}

function onSelect(event) {
	let item = event.items[0];
	console.debug(item);
}

function expandToggle(btn) {
	$timeline.classList.toggle('expanded');
	btn.innerHTML = btn.innerHTML == "&gt;" ? "&lt;" : "&gt;";
}

async function init() {
	document.head.innerHTML += Templates.providersCSS(Object.values(providers))
	await init_timeline();
	init_form();
}

window.onload = function () {
	init();
};
