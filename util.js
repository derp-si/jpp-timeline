function noop(strings, ...exp) {
	const lastIndex = strings.length - 1;

	if (!lastIndex) return strings[0];
	let acc = '', part;

	for (let i = 0; i < lastIndex; i++) {
		part = strings[i];
		if (part) acc += part;
		acc += exp[i];
	}
	part = strings[lastIndex];
	return part ? acc += part : acc;

}
html = noop;
css = noop;

function csv_parse(bufferString) {
	var arr = bufferString.split('\n');

	var jsonObj = [];
	var headers = arr[0].split(',');
	for (var i = 1; i < arr.length; i++) {
		var data = arr[i].split(',');
		var obj = {};
		for (var j = 0; j < Math.min(headers.length, data.length); j++) {
			obj[headers[j].trim()] = data[j].trim();
		}
		jsonObj.push(obj);
	}
	return jsonObj;
}

function iso_date(date) {
	return date.toISOString().split("T")[0]
}

function el_option(key, val) {
	var option = document.createElement("option");
	option.value = key;
	option.text = val;
	return option;
}

var percentColors = [
	{ pct: 0.0, color: { r: 0xff, g: 0x00, b: 0 } },
	{ pct: 0.5, color: { r: 0xff, g: 0xff, b: 0 } },
	{ pct: 1.0, color: { r: 0x00, g: 0xff, b: 0 } }
];

function getColorForPercentage(pct) {
	for (var i = 1; i < percentColors.length - 1; i++) {
		if (pct < percentColors[i].pct) {
			break;
		}
	}
	var lower = percentColors[i - 1];
	var upper = percentColors[i];
	var range = upper.pct - lower.pct;
	var rangePct = (pct - lower.pct) / range;
	var pctLower = 1 - rangePct;
	var pctUpper = rangePct;
	var color = {
		r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
		g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
		b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
	};
	return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
};

Array.prototype.range = function() {

    var min = null,
        max = null,
        i, len;

    for (i = 0, len = this.length; i < len; ++i)
    {
        var elem = this[i];
        if (min === null || min > elem) min = elem;
        if (max === null || max < elem) max = elem;
    }

    return { min: min, max: max }
};

function icsDate(d) {
	return d.getUTCFullYear() + 
	("" + (d.getUTCMonth()+1)).padStart(2,"0") + 
	("" + (d.getUTCDate())).padStart(2,"0") +
	"T" +
	("" + (d.getUTCHours())).padStart(2,"0") + 
	("" + (d.getUTCMinutes())).padStart(2,"0") +
	"00Z"
}

function downloadString(text, fileType, fileName) {
	var blob = new Blob([text], { type: fileType });
  
	var a = document.createElement('a');
	a.download = fileName;
	a.href = URL.createObjectURL(blob);
	a.dataset.downloadurl = [fileType, a.download, a.href].join(':');
	a.style.display = "none";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
	setTimeout(function() { URL.revokeObjectURL(a.href); }, 1500);
  }