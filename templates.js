const Templates = {

    duration(seconds) {
        let min = Math.floor(seconds / 60);
        let h = Math.floor(min / 60);
        seconds = seconds % 60;
        min = min % 60;
        h = h % 60;
        let str = ""
        if (h > 0)
            str += h + "h "
        if (min > 0 || h > 0)
            str += min + "min "
        if (seconds > 0)
            str += seconds + "s"
        return str;
    },

    item (item, element, data) {
        let {ride, centerText} = data;
        let start = new Date(ride.odhod);
        let end = new Date(ride.prihod);
        let code = html`
            <span class="item-left">${start.getHours().toString().padStart(2, "0")}:${start.getMinutes().toString().padStart(2, "0")}</span>
            <span class="item-center">${centerText}</span>
            <span class="item-right">${end.getHours().toString().padStart(2, "0")}:${end.getMinutes().toString().padStart(2, "0")}</span>
        `;

        let $el = document.createElement("div");
        $el.innerHTML = code;
        $el.classList.add("item");
        if (ride.opozorila.length > 0)
            $el.classList.add("warning")
        
        return $el;
    },

    tooltip (data) {
        let {ride} = data;
        let el = "";

        if (ride.opozorila.length > 0)
            for (let opozorilo of ride.opozorila)
                el += html`<strong class="warning">⚠ ${opozorilo}</strong><br>`
        if (ride.naziv)
            el += html`<strong>${ride.naziv}</strong><br>`;
        if (ride.prevoznik)
            el += html`Prevoznik: ${ride.prevoznik}<br>`;
        if (ride.peron)
            el += html`Peron: ${ride.peron}<br>`;
        if (ride.razdalja)
            el += html`Razdalja: ${ride.razdalja} km<br>`;
        if (ride.trajanje)
            el += html`Trajanje: ${ride.trajanje.toFixed(0)} min<br>`;
        if (ride.cena)
            el += html`Cena: ${ride.cena.toFixed(2)} €<br>`;
        
        el += html`Vir: ${ride.provider.provider_name}<br>`;

        return el;
    },

    group(group) {
        let $group = document.createElement("div");
        $group.innerHTML = html`
            <span class="group-label">${group.params.from.name} - ${group.params.to.name}</span><button onclick="deleteGroup('${group.id}')">×</button>
        `;
        return $group;
    },

    selectItem(station) {
        let h = `${station.name}`;
        for (let provider of Object.values(providers)) {
            if (station[provider.column_name + "_id"])
                h += html`<span class="badge ${provider.column_name}" title="${provider.provider_name}"></span>`
        }

        return h;
    },

    providersCSS(providers) {
        let el = '<style>';
        for (let provider of providers)
            el += css`
            .badge.${provider.column_name} {
                background-color: ${provider.color};
            }
            .badge.${provider.column_name}::before {
                content: "${provider.initial}";
            }`;
        el += '</style>'
        return el;
    },

    icalFile(rides) {
        let ics = `\
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//oJPP-Timetable`
        for (let ride of rides) {
            ics +=`
BEGIN:VEVENT
DTSTAMP:${icsDate(ride.odhod)}
DTSTART:${icsDate(ride.odhod)}
DTEND:${icsDate(ride.prihod)}
SUMMARY:${ride.naziv}`;
        }
        ics += `
END:VEVENT
END:VCALENDAR`;
        return ics;
    }

}