class Provider {
    api_url = "https://api.modra.ninja"

    api_name = undefined
    column_name = undefined
    provider_name = undefined
    type = "bus"

    items = [];

    async fetch(from, to, date) {
        this.items = []
        let data = await fetch(this.api_url + `/${this.api_name}/vozni_red/${from}/${to}/${iso_date(date)}`);
        data = await data.json();
        return data;
    }

    async loadDataset(params) {
        let data = await this.fetch(params.from[this.column_name + "_id"], params.to[this.column_name + "_id"], params.date);
        
        for (let ride of data) {
            ride.odhod = new Date(Date.parse(ride.odhod));
            ride.prihod = new Date(Date.parse(ride.prihod));
            ride.trajanje = (ride.prihod - ride.odhod) / 1000 / 60
            ride.provider = this;
        }
        data = this.process_rides(data);

        for (let ride of data) {
            this.add_item({
                start: ride.odhod,
                end: ride.prihod,
                group: params.groupId,
                ride: ride,
                centerText: Templates.duration(ride.trajanje * 60),
            }, params);
        }
        this.process_items();
        return this.items;
    }

    is_loaded() {
        return this.items.length > 0
    }

    filter_item(item) {return true}
    process_rides(d) {return d}
    process_items(d) {return d}

    add_item(item, params) {
        // console.debug(item, params)
        item.provider = this.column_name;
        this.items.push(item);
    }
}

const providers = {
	aplj: new class APLJ extends Provider {
		provider_name = "AP Ljubljana"
        api_name = "ap_ljubljana"
        column_name = "aplj"
        initial = "LJ"
        color = "#0056a1"
        type = "bus"
        add_item(item, params) {
            super.add_item(item, params)
        }
        exclusion_map = {
            'NOMAGO, storitve mobilnosti in potovanj, d.o.o.': 'nomago',
            'AP Murska Sobota d.d.': 'apms',
            'ARRIVA d.o.o.': 'arriva',
        }
        filter_item(item) {
            if (item.provider === 'aplj') {
                let other = this.exclusion_map[item.ride.prevoznik];
                if (other !== undefined && providers[other].is_loaded())
                    return false;
            }
            return true;
        }
	}(),
	sz: new class SZ extends Provider {
		provider_name = "Slovenske Železnice"
        api_name = "sz"
        column_name = "sz"
        initial = "SŽ"
        color = "#00a4e6"
        type = "train"
        add_item(item, params) {
            if (!item.ride.redundant)
                super.add_item(item, params)
        }
        process_rides(rides) {
            for (let ride of rides) 
                ride.parts = ride.naziv.split(', ');

            for (let ride of rides) {
                for (let ride2 of rides) {
                    
                    if (ride2.trajanje > ride.trajanje) {
                        if (ride2.parts[ride2.parts.length-1] == ride.parts[ride.parts.length-1]) {
                            ride2.redundant = true;
                        }
                    }
                }
            }
            return rides;
        }
	}(),
    arriva: new class Arriva extends Provider {
		provider_name = "Arriva"
        api_name = "arriva"
        column_name = "arriva"
        initial = "A"
        color = "#1cb3c5"
        type = "bus"
        add_item(item, params) {
            super.add_item(item, params)
        }
	}(),
    nomago: new class Nomago extends Provider {
		provider_name = "Nomago"
        api_name = "nomago"
        column_name = "nomago"
        initial = "N"
        color = "#fbba00"
        type = "bus"
        add_item(item, params) {
            if (item.ride.prevoznik == "Arriva d.o.o." || item.ride.prevoznik == "Avtobusni promet Murska Sobota d.d.")
                return;
            super.add_item(item, params)
        }
        exclusion_map = {
            'Avtobusni promet Murska Sobota d.d.': 'apms',
            'Arriva d.o.o.': 'arriva',
        }
        filter_item(item) {
            if (item.provider === 'aplj') {
                let other = this.exclusion_map[item.ride.prevoznik];
                if (other !== undefined && providers[other].is_loaded())
                    return false;
            }
            return true;
        }
	}(),
    apms: new class APMS extends Provider {
		provider_name = "AP Murska Sobota"
        api_name = "apms"
        column_name = "apms"
        initial = "MS"
        color = "#0077be"
        type = "bus"
	}(),
	prevoz: new class Prevoz extends Provider {
		provider_name = "Prevoz.org"
        api_name = "prevoz"
        column_name = "prevoz"
        initial = "P"
        color = "#ec903b"
        type = "rideshare"
	}(),
	// apmb: new class APMB extends Provider {
	// 	provider_name = "AP Maribor"
    //     api_name = "ap_maribor"
    //     column_name = "apmb"
    //     add_item(item, params) {
    //         item.className = "bus"
    //         super.add_item(item, params)
    //     }
	// }(),
}
