#!/bin/sh
cd "$(dirname "$0")/.." || exit 1

echo
echo "Downloading dependencies..."

mkdir -p vendor
aria2c -i vendor.txt -d vendor/ --auto-file-renaming=false

echo
echo "Generating station map..."

cd scripts/
./update_station_map.sh
cd ../

echo
echo "DONE!"
