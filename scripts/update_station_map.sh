#!/bin/bash
cd "$(dirname "$0")" || exit 1


# Open SSH tunnel
ssh -f -N -L 3337:localhost:3306 cabbage.franga2000.com

# Dump DB
mariadb \
        --defaults-file='creds.my.cnf' \
        --batch \
        --column-names \
        < 'station_map_joined.sql' > '../data/station_map_joined.tmp.csv'

success=$?

# Close SSH tunnel
kill $(ps aux | grep 'ssh -f -N -L 3337:localhost:3306 cabbage.franga2000.com' | awk '{print $2}')

if [ $success -eq 0 ]; then

# Fix CSV
cat '../data/station_map_joined.tmp.csv' \
    | sed 's/\t/,/g' \
    | sed 's/NULL//g' \
        > '../data/station_map_joined.csv'
fi

# Remove temp file
rm '../data/station_map_joined.tmp.csv'
