SELECT * 
    FROM station_map 
        NATURAL LEFT JOIN sz_stations 
        NATURAL LEFT JOIN sz_eshop_stations 
        NATURAL LEFT JOIN aplj_stations 
        NATURAL LEFT JOIN apmb_stations
        NATURAL LEFT JOIN arriva_stations
        NATURAL LEFT JOIN nomago_stations
        NATURAL LEFT JOIN apms_stations
        NATURAL LEFT JOIN prevoz_stations;
